// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import axios from "axios";
import template from "../../data.json";

const link =
  "https://gitlab.com/mekagojira/privacy-replacement/-/raw/main/data.json";

const parser = (data) => {
  if (!Array.isArray(data)) return template;
  if (
    !data.every((item) => {
      return item?.evil && Array.isArray(item?.replacement);
    })
  )
    return template;

  return data.map(({ evil, replace, comment }) => {
    return {
      evil,
      replacement,
      comment: comment || "",
    };
  });
};

export default function handler(req, res) {
  axios(link)
    .then(({ data }) => {
      res.status(200).json(parser(data));
    })
    .catch(() => {
      res.status(200).json(parser(null));
    });
}
