import Link from "next/link";
import { useEffect, useState } from "react";

const Home = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch("/api/data")
      .then((res) => res.json())
      .then((resp) => {
        setData(resp);
      });
  }, []);

  const renderLink = (link) => {
    if (!link?.url || !link?.text)
      return <span className="my-1 font-bold text-slate-100">{link}</span>;

    return (
      <Link href={link?.url}>
        <a
          className="my-1 font-bold text-slate-100 hover:text-cyan-500 transition-all"
          target="__blank"
        >
          {link.text}
        </a>
      </Link>
    );
  };

  const renderRow = ({ evil, replacement, comment = "" }, i) => {
    const isLast = i === data.length - 1;

    return (
      <div
        key={i}
        className="flex flex-wrap bg-slate-800 hover:bg-slate-600 transition-all"
      >
        <div
          className={
            "w-1/3 p-2 flex flex-col justify-center items-center" +
            (isLast ? " rounded-bl-lg" : "")
          }
        >
          {renderLink(evil)}
        </div>
        <div className="w-1/3 p-2 flex flex-col justify-center items-center">
          {replacement.map((item, j) => {
            return <span key={`${i}.${j}`}>{renderLink(item)}</span>;
          })}
        </div>
        <div
          className={
            "w-1/3 p-2 flex flex-col justify-center items-center" +
            (isLast ? " rounded-br-lg" : "")
          }
        >
          {comment}
        </div>
      </div>
    );
  };

  return (
    <div className="min-h-screen bg-slate-700 text-white">
      <div className="container mx-auto p-4">
        <div className="flex flex-wrap">
          <div className="border-b border-slate-700 w-1/3 bg-slate-800 p-2 flex flex-col justify-center items-center rounded-tl-lg">
            Evil software
          </div>
          <div className="border-b border-slate-700 w-1/3 bg-slate-800 p-2 flex flex-col justify-center items-center">
            Replacement
          </div>
          <div className="border-b border-slate-700 w-1/3 bg-slate-800 p-2 flex flex-col justify-center items-center rounded-tr-lg">
            Comment
          </div>
        </div>
        {data.map(renderRow)}
      </div>
    </div>
  );
};

export default Home;
